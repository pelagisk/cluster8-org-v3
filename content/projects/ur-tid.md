---
title: Ur Tid
date: 2023-05-10T09:03:20-08:00
---

### A sketch is a world, flattened

2023 \
*Intaglio prints, microscopy footage shown on a Raspberry Pi 4 and stereoscope with procedurally computer-generated pollen grains.*

{{< imgrow >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846396/Ur%20Tid/11_labfmq.jpg" alt="Description must be added here ASAP!" >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846396/Ur%20Tid/12_izyyjy.jpg" alt="Description must be added here ASAP!" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846396/Ur%20Tid/15_gsms4j.jpg" alt="Description must be added here ASAP!" >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846396/Ur%20Tid/16_niks0c.jpg" alt="Description must be added here ASAP!" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846396/Ur%20Tid/13_s3ltht.jpg" alt="Description must be added here ASAP!" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846399/Ur%20Tid/32_tqxzu4.jpg" alt="Description must be added here ASAP!" >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846399/Ur%20Tid/33_mlzfd5.jpg" alt="Description must be added here ASAP!" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846402/Ur%20Tid/55_p4zsnj.jpg" alt="Description must be added here ASAP!" >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846402/Ur%20Tid/56_akockk.jpg" alt="Description must be added here ASAP!" >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846403/Ur%20Tid/57_nzbcfx.jpg" alt="Description must be added here ASAP!" >}}
{{< /imgrow >}}

With *A sketch is a world, flattened*, Axel Gagge reflects on different ways we can observe and perceive pollen. Margaret McFall-Ngai argues that “Our understanding of the biological world has always been fundamentally linked to how we are able to perceive it, and what we can perceive is tied to the technologies we have for seeing” (2017, pp. 52-53). Here Axel Gagge makes use of scientific methods for seeing, like a microscope or an observational drawing, while simultaneously finding ways to rethink these methods more poetically.

### Column – chorus

2023 \
*LED strip with color patterns visualizing the changing ratios of prehistoric pollen grains in a lake.*

{{< imgrow >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846397/Ur%20Tid/22_nsgnty.jpg" alt="Description must be added here ASAP!" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846397/Ur%20Tid/23_jklqtp.jpg" alt="Description must be added here ASAP!" >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846397/Ur%20Tid/24_qdjtgc.jpg" alt="Description must be added here ASAP!" >}}
  {{< img src="https://res.cloudinary.com/dm3mpw8cj/image/upload/v1696846397/Ur%20Tid/25_zpujkz.jpg" alt="Description must be added here ASAP!" >}}
{{< /imgrow >}}

A light installation in which each color of LED light corresponds to the number of pollen grains of birch, fir, lark and oak for each season in a particular lake. The numbers were found by drilling and extracting a sample from the sediments and counting pollen husks with a microscope. In this way, a history of the vegetation around the lake is told. Gagge translates the rhythm and harmony of past ecosystems into light. Pollen grains are vestiges of the past. When we find them in sediment, they become kernels of knowledge of another time, and the potential for new life as a new plant will emerge, in a new time. They are the time-travellers, holding endless potential for renewal. Pollen reveals our ecosystems – like our DNA, it holds the code to understand the biodiversity and composition of our environment at a given moment. But as we increasingly destroy species, plants and their pollen disappear. What we find in the ground are ghosts of the Holocene, specks of an ecosystem that is fading into mere memory. In pollen, time goes fuzzy: it contains so much beyond us, into the past and the future.