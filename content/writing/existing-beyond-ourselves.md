---
title: Existing Beyond Ourselves
author: Isabelle Ribe
date: 2021-10-21T09:03:20-08:00

---

*Written by Isabelle Ribe*

While spending time with the Gustav Sandberg archive, I developed a visceral awareness that I am, for once, allowed to touch historical remnants of someone else’s life with my bare hands. I had previously conceived of the archive as something inherently experienced through either a glass display case or hypoallergenic latex gloves. It had this aura of the unattainable, reflecting a frenetic human need to preserve as much as possible - even if it means we won’t really be able to get close for fear of destroying it. During my past experiences in archives, it felt like I was trespassing, looking for the piece of information I needed and leaving immediately. In Sandberg’s archive however, I felt invited as a co-custodian who could take her time with the material. There is now a layer of myself added to this archive. I left traces behind, from my fingerprints to the personal notes I took. While there is great beauty and knowledge to be found in these new layers of interpretation, this process of inscription through research also begs the following questions: What makes me worthy to leave these traces of myself on someone else’s life? Am I erasing Sandberg’s agency by superimposing my own onto his research?

Reflecting on this layering effect upon the archive as it moves into the care of different people, I was reminded of a talk by Fiona Mackenzie, the archivist at Canna House in the Scottish Hebrides. Canna House was the house of Margaret Fay Shaw and her husband John Lorne Campbell. They were fascinated by folklore and language, and gathered an immense amount of material on Hebridean culture. Margaret Fay Shaw grew up in New York with a keen interest in music, but she became fascinated by Scottish culture during a visit. In 1929 she decided to settle permanently in South Uist, and she spent her life manually transcribing Gaelic folk songs around the Hebrides. When the Campbells died, a woman named Magda Sagarzazu became the caretaker of their house. Magda was from the Basque country, and had spent her teen years with the Campbells. She devoted her life to a lifestyle and language that weren’t her own, and her Spanish heritage became completely integral to Canna House. Fiona Mackenzie talked about finding notes and labels in Spanish around the house when she took over from Magda, and she learned Spanish to be able to fully understand the archive. The layering of Gaelic, English and Spanish enriched the house immensely, as each language contains ways of thinking that cannot be fully translated into another language. Fiona passionately insisted on the importance of recording lives, as folklore is an ongoing process. As a musician herself, she has been continuing to gather more recent stories and music from the Hebrides, and she sang some folk songs for us, keeping the archive alive through the vibrations of her voice. She was convinced that an archive is useless if you don’t do anything with it. Why keep it if no one can experience it?

I thought of conceptual artist Ilya Kabakov’s 1977 text “The Man Who Never Threw Anything Away”. The narrator tells the story of a mysterious plumber who lived in his family’s apartment and hoarded an immense amount of “stuff” in his room. In a manuscript titled “Garbage”, the plumber argues the following:

“To deprive ourselves of these paper symbols and testimonies is to deprive ourselves somewhat of our memories. In our memory everything becomes equally valuable and significant. All points of our recollections are tied to one another. They form chains and connections in our memory which ultimately comprise the story of our life. To deprive ourselves of all this means to part with who we were in the past, and in a certain sense, it means to cease to exist.” (Kabakov, 1977, p. 33)

I find it interesting to describe the archival drive as such a fierce attempt to continue existing beyond our own ephemerality. But what really resonated with me was the description of the interconnection and lack of strict hierarchy among our memories. During our exploration of the Sandberg archive, we were constantly trying to find a system or crack a code, but we realised that we couldn’t actually judge what was important; everything seemed equally interesting and unfathomable. Kabakov’s plumber goes on to argue that “grouped together, bound in folders, these papers comprise the single uninterrupted fabric in an entire life, the way it was in the past and the way it is now.” (1977, p. 34). This created an interesting contradiction to Foucault’s description of the archive in “The Historical a priori and the Archive” as something that “deprives us of our continuities” (1969, p. 30), reflecting a more pessimistic (realistic?) way of looking at an entity that is, in its essence, composed of fragments. A second manuscript by the plumber, called “A Dump”, explores the idea of the archive as trash, but without the usual disdain directed at unwanted things:

“The whole world, everything which surrounds me here, is to me a boundless dump with no ends or borders, an inexhaustible, diverse sea of garbage. In this refuse of an enormous city one can feel the powerful breathing of its entire past. This whole dump is full of twinkling stars, reflections and fragments of culture.” (Kabakov, 1977, p. 35)

It is fascinating that Kabakov creates a poetic vision of what we discard. Our whole human existence is eventually abandoned, forgotten, decomposed. The concept of the dump flattens hierarchies, and allows for finding value in all things, no matter how lowly they seem. The Sandberg archive could appear to be some random scientist’s trash at first, but if we spend proper time with these records, striving to understand them, the poetics appear out of the garbage.


**Bibliography:**

Foucault, M. (1969). *The Historical a priori and the Archive.*
In C. Merewether (Ed.). *The Archive* (pp. 26-30).
London: Whitechapel Gallery.

Kabakov, I. (1977). *The Man Who Never Threw Anything Away.*
In C. Merewether (Ed.). *The Archive* (pp. 32-37).
London: Whitechapel Gallery.
