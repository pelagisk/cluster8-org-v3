---
title: Data and ruins
author: Axel Gagge
date: 2021-10-21T09:03:20-08:00
---

*Written by Axel Gagge*

I like to think of “data” as a building block, a contemporary construction material. Data consists of “data points”, information which someone has collected and ordered into a meaningful whole. The ordering of data is a creative and intellectual search for structure. It involves both mathematical methods and artistic visualization. Sometimes, it is called data exploration. In almost every criminal drama, there is a scene of the cops finally finding the crucial piece of evidence in their databases.

We created many 3D scans during our field trips. In digital 3D models, data takes the form of “point clouds”, where each data point represents a physical position in space. The points are connected into triangles to form surfaces which are called “meshes”. In a sense, a 3D modeller “sees” the world as just a set of points connected into triangles. A reductionist but convenient perspective, since it lets us share, combine and change 3D shapes however we want.

This way of seeing is also the characteristic perspective of the data scientist. We might be working with atoms, cells, statistics or currency, but we only see data points. What data is and is not collected? How do I order my data and what is visible in that order? Data scientists struggle to understand their own perspective, to see what they cannot see. It is only this ongoing process which merits the name “scientific”. Humans have a long history of being limited by our own assumptions about the world, and I believe we have a long way to go before we understand the assumptions behind data.

Buildings are abandoned and turn into ruins: plants and animals soon reclaim the sites. In the same way, collections of data are already abandoned, their order forgotten. This was at the back of my mind when we visited the beautiful ruins of Sigtuna, Haga castle and Kymlinge. In the crude 3D scans we collected there, I began to appreciate data as ruin and vice versa.
