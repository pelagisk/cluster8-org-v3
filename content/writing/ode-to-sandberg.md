---
title: Ode To Sandberg
author: Lior Nønne Malue Hansen
date: 2021-10-21T09:03:20-08:00
---

*Written by Lior Nønne Malue Hansen*

We opened box upon box \
Of culturally wrapped natural material \
Allowing the sediments \
Of your fragmented collection \
To pollenize the ruin in the make \
Of our (con)temporary hive mind \
We layed out all of our perceptions \
On a thousand broken plateaus \
In an effort to connect the dots \
We puzzled them into an imagery \
That felt like a bad compromise \
With Deleuze as our tour guide \
The rhizome unfolded \
An incomplete data set \
Set out to date \
A point in time forever lost