---
title: About
date: 2022-11-20T09:03:20-08:00
---
Cluster 8 is an interdisciplinary art collective of curatorial practice, art history, quantum physics, and artistic creation. Using experimentation as a form, Cluster 8 explores the borders where art and research meets: between technology and nature, digital process and analog work.

The collective consists of Isabelle Ribe, curator; Sara Ekholm Eriksson, artist; Lior Nønne Malue Hansen, artist; Cluster 8, artist and researcher in theoretical quantum physics; and Otto Ruin, art critic and writer.